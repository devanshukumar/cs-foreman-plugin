require File.expand_path('../lib/testpro/version', __FILE__)
require 'date'

Gem::Specification.new do |s|
  s.name        = 'testpro'
  s.version     = Testpro::VERSION
  s.date        = Date.today.to_s
  s.authors     = ['abhi']
  s.email       = ['abhi@abhi.com']
  s.homepage    = 'http://cloudfx.com'
  s.summary     = 'Summary of Testpro.'
  # also update locale/gemspec.rb
  s.description = 'Description of Testpro.'

  s.files = Dir['{app,config,db,lib,locale}/**/*'] + ['LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'deface'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rdoc'
end
