class CreateCloudDatabases < ActiveRecord::Migration
  def change
    create_table :cloud_databases do |t|
      t.string :db_name
      t.integer :allocated_storage
      t.string :db_instance_class
      t.string :engine
      t.string :master_username
      t.string :master_user_password
      t.string :storage_type
      t.string :db_id
      t.string :compute_resource_id
      t.string :organization_id

      t.timestamps null: false
    end
  end
end
