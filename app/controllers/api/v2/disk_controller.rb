require 'fog/azurerm'
module Api
  module V2
    class DiskController < ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)

        cr = ComputeResource.find(id);
        return cr

      end

      def destroy
        begin
          data = params.permit(:resource_group, :id, :compute_resource_id)

          cr = getComputeResourceValue(data[:compute_resource_id])
          fog_compute_service = Fog::Compute::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud'

          )
          managed_disk = fog_compute_service
                             .managed_disks
                             .get(data[:resource_group], data[:id])
          ::Foreman::Logging::logger('testpro').info "#{managed_disk.name}"
          managed_disk.destroy


          render json: {status: 'SUCCESS', message: 'Disk Deleted'}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in deletion in Disk', data: e}, status: :ok

          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def create
        begin

          data = params.permit(:resource_group, :location, :name, :disk_size_in_gb, :compute_resource_id)
          cr = getComputeResourceValue(data[:compute_resource_id])

          fog_compute_service = Fog::Compute::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud'

          )
          resp = fog_compute_service.managed_disks.create(
              resource_group_name: data[:resource_group], #'CFXtest',
              location: data[:location], #'southcentralus',
              name: data[:name], #'diskilo2',
              disk_size_gb: data[:disk_size_in_gb], # '30',
              creation_data: {
                  create_option: 'empty'
              }
          )


          render json: {status: 'SUCCESS', message: 'Disk Created', response: resp}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in creation in Disk', data: e}, status: :ok

          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end

