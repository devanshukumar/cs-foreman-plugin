require 'aws-sdk-redshift'

module Api
  module V2
    class AmazonredshiftclusterController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def create
        data = params.permit(:compute_resource_id, :db_name, :cluster_identifier, :cluster_type, :node_type, :master_username, :master_user_password, :cluster_security_groups,
                             :vpc_security_group_ids, :cluster_subnet_group_name, :availability_zone, :preferred_maintenance_window, :cluster_parameter_group_name, :automated_snapshot_retention_period,
                             :port, :allow_version_upgrade, :number_of_nodes, :publicly_accessible, :encrypted, :hsm_client_certificate_identifier, :hsm_configuration_identifier,
                             :elastic_ip, :kms_key_id, :enhanced_vpc_routing, :maintenance_track_name,
                             :compute_resource_id, :organization_id)
        begin
          client = create_connection(data)
          resp = client.create_cluster({
                                           db_name: data[:db_name],
                                           cluster_identifier: data[:cluster_identifier],
                                           cluster_type: data[:cluster_type],
                                           node_type: data[:node_type],
                                           master_username: data[:master_username],
                                           master_user_password: data[:master_user_password],
                                           port: data[:port],
                                           tags: [{key: 'Name', value: data[:cluster_identifier] + data[:region], },],
                                       }
          )
          ::Foreman::Logging::logger('testpro').info resp
          render json: {status: 'SUCCESS', message: 'RedShift Created', Response: resp.to_hash.to_json}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While Creating RedShift due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      private def create_connection(data)
        cr = cr = ComputeResource.find(data[:compute_resource_id]);
        client = Aws::Redshift::Client.new(:access_key_id => cr.user,
                                           :secret_access_key => cr.password,
                                           :region => cr.url)
        return client
      end

      def destroy
        data = params.permit(:cluster_identifier, :cluster_snapshot, :snapshot_identifier, :compute_resource_id)
        begin
          client = create_connection(data)
          resp = client.delete_cluster({
                                           cluster_identifier: data[:cluster_identifier],
                                           skip_final_cluster_snapshot: data[:cluster_snapshot],
                                           final_cluster_snapshot_identifier: data[:snapshot_identifier]
                                       })
          render json: {status: 'SUCCESS', message: 'RedShift Deleted', response: resp.to_hash.to_json}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While Deleting RedShift due to #{e.message}"}, status: :unprocessable_entity
        end
      end
    end
  end
end

