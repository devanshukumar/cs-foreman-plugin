require 'aws-sdk-acm'
module Api
  module V2
    class CertificatesController < ApplicationController
      skip_before_action :verify_authenticity_token

      def get_resource(id)
        cr = ComputeResource.find(id)
        return Aws::ACM::Client.new(:access_key_id => cr.user,
                                    :secret_access_key => cr.password, :region => cr.url)
      end

      def create
        begin
          client = get_resource(params[:compute_resource_id])
          cert_data = {
              domain_name: params[:domain_name], # required
              validation_method: params[:validation_method], # accepts EMAIL, DNS
              idempotency_token: "IdempotencyToken",
              # domain_validation_options: [
              #     {
              #         domain_name: "DomainNameString", # required
              #         validation_domain: "DomainNameString", # required
              #     },
              #        ],
              options: {
                  certificate_transparency_logging_preference: "ENABLED", # accepts ENABLED, DISABLED
              }
              #,
              #certificate_authority_arn: "Arn",
          }
          response = client.request_certificate(cert_data)
          name_tag = client.add_tags_to_certificate({
                                                        certificate_arn: response.certificate_arn, # required
                                                        tags: [# required
                                                            {
                                                                key: "Name", # required
                                                                value: params[:name],
                                                            },
                                                        ],
                                                    })

          render json: {status: 'SUCCESS', message: 'certificate Request Created', response: response.to_hash}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'certificate Request not created', data: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def destroy
        begin
          client = get_resource(params[:compute_resource_id])
          response = client.delete_certificate({
                                                   certificate_arn: params[:arn], # required
                                               })
          render json: {status: 'SUCCESS', message: 'certificate Request Deleted', response: response.to_hash}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'certificate Request not Deleted', data: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end
