require 'fog/azurerm'

module Api
  module V2
    class AwsController < ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)

        cr = ComputeResource.find(id);
        return cr

      end

      def createUserLogin
        begin
          data = params.permit(:iam_username, :iam_password, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          iam = Aws::IAM::Client.new(:access_key_id => cr.user, :secret_access_key => cr.password, :region => cr.url)


          user = iam.create_user(user_name: data[:iam_username])
          iam.wait_until(:user_exists, user_name: data[:iam_username])

          iam.create_login_profile({password: data[:iam_password], user_name: data[:iam_username]})

#          arn_parts = iam.arn.split(':')
#         puts 'Account ID:        ' + arn_parts[4]
          render json: {status: 'SUCCESS', message: 'IAM User Created', response: 'User Created'}, status: :ok
        rescue Aws::IAM::Errors::EntityAlreadyExists
          render json: {status: 'FAILURE', message: 'IAM User already exists', response: "#{e}"}
        end
      end


      def listS3BucketKeys
        begin
          cr = ComputeResource.find_by type: 'Foreman::Model::EC2'
          if params.has_key? :region
            region = params[:region]
          else
            region = cr.url
          end
          s3 = Aws::S3::Resource.new(:access_key_id => cr.user, :secret_access_key => cr.password, :region => region)

          data = params.permit(:bucket_name)
          bucket = s3.bucket(data[:bucket_name])
          s3bucketKeys = Array.new([])

          bucket.objects.limit(50).each do |item|
            s3bucketKeys.push(item.key)
          end
          render json: {status: 'SUCCESS', message: 'S3 Key Listing', response: s3bucketKeys}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in listing Keys  S3', response: "#{e}"}
        end
      end

      def createEFS
        begin
          data = params.permit(:creation_token, :performance_mode, :file_system_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          efs = Aws::EFS::Client.new(:access_key_id => cr.user, :secret_access_key => cr.password, :region => cr.url)

          resp = efs.create_file_system({creation_token: data[:creation_token], performance_mode: data[:performance_mode]})

          resp_2 = efs.create_tags({
                                       file_system_id: resp.file_system_id,
                                       tags: [
                                           {
                                               key: 'Name',
                                               value: data[:file_system_name],
                                           },
                                       ],
                                   })

          render json: {status: 'SUCCESS', message: 'EFS Created', response: "#{resp}"}
        rescue Exception => e
          render json: {status: 'ERROR', message: 'ERROR in Creating EFS', data: "#{e}"}
        end
      end


      def listS3Bucket
        begin
          cr = ComputeResource.find_by type: 'Foreman::Model::EC2'
          s3 = Aws::S3::Client.new(:access_key_id => cr.user, :secret_access_key => cr.password, :region => cr.url)

          resp = s3.list_buckets
          s3buckets = Array.new([])
          resp.buckets.each do |bucket|
            if params.has_key? :region
              bucket_location = s3.get_bucket_location({bucket: bucket.name}).location_constraint
              if bucket_location == params[:region]
                s3buckets.push(bucket.name)
              end
            else
              s3buckets.push(bucket.name)
            end
          end

          render json: {status: 'SUCCESS', message: 'S3 Listing', response: s3buckets.to_json}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in listing S3', response: e.to_json}
        end
      end

      def createStorageBucket
        begin
          data = params.permit(:bucket_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])

          s3 = Aws::S3::Client.new(:access_key_id => cr.user,
                                   :secret_access_key => cr.password, :region => cr.url

          )

          resp = s3.create_bucket(bucket: data[:bucket_name])
          render json: {status: 'SUCCESS', message: 'S3 Created', response: resp.to_json}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in creation S3', response: "#{e}"}
        end
      end

    end
  end
end



