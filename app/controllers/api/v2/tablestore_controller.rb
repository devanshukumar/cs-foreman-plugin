require 'azure/storage'

module Api
  module V2
    class TablestoreController < ::ApplicationController
      skip_before_action :verify_authenticity_token
      #this method will create the table storage based 
      def create
        data = params.permit(:resource_group_name, :storage_account, :table_name, :end_point_url, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection(data)
          storage_acc = fog_storage_service.storage_accounts.get(data[:resource_group_name], data[:storage_account])
          keys_hash = storage_acc.get_access_keys
          storage_access_key = keys_hash[0].value

          tablestore = Azure::Storage::Client.create(:storage_account_name => data[:storage_account], :storage_access_key => storage_access_key)
          tables = tablestore.table_client
          tables.with_filter(Azure::Storage::Core::Filter::ExponentialRetryPolicyFilter.new)
          tables.create_table(data[:table_name])
          render json: {status: 'SUCCESS', message: 'Successfully Table Storage Created', Response: data}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Table Storage not Created due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will create the connection with existing storage account
      private def create_connection(data)
        cr = ComputeResource.find(data[:compute_resource_id])
        fog_storage_service = Fog::Storage::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_storage_service
      end

      #this method will deleted the table storage base on 'resource group' and storage 'account name'
      def destroy
        data = params.permit(:storage_account, :resource_group_name, :storage_access_key, :table_name, :end_point_url, :compute_resource_id, :organizaion_id)
        begin
          fog_storage_service = create_connection(data)
          storage_acc = fog_storage_service.storage_accounts.get(data[:resource_group_name], data[:storage_account])
          keys_hash = storage_acc.get_access_keys
          storage_access_key = keys_hash[0].value
          tablestore = Azure::Storage::Client.create(:storage_account_name => data[:storage_account], :storage_access_key => storage_access_key)
          tables = tablestore.table_client
          tables.delete_table(data[:table_name])
          render json: {status: 'SUCCESS', message: 'Table Storage Deleted Successfully', Response: data}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Table Storage not Deleted due to #{e.message}"}, status: :unprocessable_entity
        end
      end
    end
  end
end

