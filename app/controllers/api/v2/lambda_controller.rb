require 'aws-sdk-lambda'
module Api
  module V2
    class LambdaController < ::ApplicationController

      skip_before_action :verify_authenticity_token


      def create
        begin
          payload = params.permit(:function_name, :runtime, :handler, :role, :code_source,
                                  :s3_bucket, :s3_key, :compute_resource_id)
          cr = ComputeResource.find(payload[:compute_resource_id])
          @client = Aws::Lambda::Client.new(:access_key_id => cr.user,
                                            :secret_access_key => cr.password, :region => cr.url)


          #@client.get_function({function_name: "myfunction"})
          #
          case payload[:code_source]
          when "S3"
            code = {
                #'ZipFile': b'bytes',
                's3_bucket': payload[:s3_bucket],
                's3_key': payload[:s3_key],
                #'s3_object_version': 'string'
            }
          else
            raise Exception("no valid source")

          end
          response = @client.create_function({runtime: payload[:runtime],
                                              role: payload[:role],
                                              handler: payload[:handler],
                                              function_name: payload[:function_name],
                                              code: code
                                             })
          ::Foreman::Logging::logger('testpro').info response
          #tagging for billing
          resp = @client.tag_resource({
                                          resource: response.function_arn, # required
                                          tags: {# required
                                                 "Name" => response.function_arn,
                                          },
                                      })
          render json: {status: 'SUCCESS', message: 'Lambda created successfully', response: response.to_hash, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok

        rescue Exception => e
          render json: {status: 'ERROR', message: "Lambda not created due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def destroy
        begin
          cr = ComputeResource.find(params[:compute_resource_id])
          @client = Aws::Lambda::Client.new(:access_key_id => cr.user,
                                            :secret_access_key => cr.password, :region => cr.url)
          response = @client.delete_function({function_name: params[:id]})
          ::Foreman::Logging::logger('testpro').info response
          render json: {status: 'SUCCESS', message: 'Lambda deleted successfully', response: response.to_hash.to_json, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok
        rescue
          render json: {status: 'ERROR', message: "Lambda not deleted due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end

  end
end

