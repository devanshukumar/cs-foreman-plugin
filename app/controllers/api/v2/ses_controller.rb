require 'aws-sdk-ses'

module Api
  module V2
    class SesController < ApplicationController
      skip_before_action :verify_authenticity_token

      def create
        begin
          payload = params.permit(:sender, :recipient, :htmlbody,
                                  :textbody, :subject, :compute_resource_id)
          cr = ComputeResource.find(payload[:compute_resource_id])
          ses = Aws::SES::Client.new(:access_key_id => cr.user,
                                     :secret_access_key => cr.password,
                                     :region => cr.url)

          email = {
              source: payload[:sender],
              destination: {to_addresses: [payload[:recipient]], },
              message: {
                  body: {
                      html: {charset: "UTF-8", data: payload[:htmlbody], },
                      text: {charset: "UTF-8", data: payload[:textbody], }, },
                  subject: {
                      charset: "UTF-8", data: payload[:subject], },
              },
              #tags: [{name: "Name",
              #        value: "#{payload[:sender]}:#{payload[:recipient]}",},],
          }
          resp = ses.send_email(email)
          render json: {status: 'SUCCESS', message: "email sent"}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "email not sent", response: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def add_domain
        begin
          payload = params.permit(:domain, :compute_resource_id)
          cr = ComputeResource.find(payload[:compute_resource_id])
          ses = Aws::SES::Client.new(:access_key_id => cr.user,
                                     :secret_access_key => cr.password,
                                     :region => cr.url)
          resp = ses.verify_domain_identity({domain: payload[:domain], })
          render json: {status: 'SUCCESS', message: "domain added", response: resp.to_h}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "domain not verified", response: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def add_email_address
        begin
          payload = params.permit(:email_address, :compute_resource_id)
          cr = ComputeResource.find(payload[:compute_resource_id])
          ses = Aws::SES::Client.new(:access_key_id => cr.user,
                                     :secret_access_key => cr.password,
                                     :region => cr.url)
          resp = ses.verify_email_address({email_address: payload[:email_address], })
          render json: {status: 'SUCCESS', message: "verification email sent"}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "verification email not sent", response: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def delete_identity
        begin
          payload = params.permit(:identity, :compute_resource_id)
          cr = ComputeResource.find(payload[:compute_resource_id])
          ses = Aws::SES::Client.new(:access_key_id => cr.user,
                                     :secret_access_key => cr.password,
                                     :region => cr.url)
          resp = ses.delete_identity({identity: payload[:identity], })
          render json: {status: 'SUCCESS', message: "identity removed"}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "identity not removed", response: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end
