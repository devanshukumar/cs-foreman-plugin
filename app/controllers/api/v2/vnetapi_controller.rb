require 'fog/azurerm'

module Api
  module V2
    class VnetapiController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def show
        vnetName = Array.new

        data = params.permit(:tenant_id, :client_id, :resource_group_name, :client_secret, :subscription_id,
                             :environment, :end_point_url, :compute_resource_id, :organization_id)
        begin
          fog_network_service = create_connection()

          vnets = fog_network_service.virtual_networks(resource_group: params[:id])
          if (params.has_key?(:location))
            vnets.each do |vnet|
              if vnet.location == params[:location]
                vnetName.push(vnet.name)
              end
            end
          else
            vnets.each do |vnet|
              vnetName.push(vnet.name)
            end
          end
          render json: {status: 'SUCCESS', message: 'List of VNetName', Response: vnetName}, status: :ok

        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While VNetName due to #{e.message}"}, status: :unprocessable_entity
        end

      end

      #this function will create the connection with existing storage account
      private def create_connection()
        cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
        fog_network_service = Fog::Network::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_network_service
      end
    end
  end
end
