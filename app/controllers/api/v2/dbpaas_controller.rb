require 'rest-client'
require 'fog/azurerm'
require 'json'
require_relative 'azure_properties'
module Api
  module V2
    class DbpaasController < ::ApplicationController

      skip_before_action :verify_authenticity_token

      def get_azure_details(id)
        cr = ComputeResource.find(id)
        @tenant_id = cr.uuid
        @subscription_id = cr.user
        @client_id = cr.url
        @client_secret = cr.password
        #####
      end

      def create


        data = params.permit(:resource_group, :server_name, :database, :location,
                             :edition, :version, :administrator_login, :administrator_login_password,
                             :db_name, :create_mode, :collation, :compute_resource_id)
        begin
          get_azure_details(data[:compute_resource_id])
          @connector = AzureConnector.new(@client_id, @client_secret, @tenant_id, @subscription_id)
          server_response = create_server data
          ::Foreman::Logging::logger('testpro').info "server creation response:" + server_response.to_s
          sleep 40
          db = create_db data
          ::Foreman::Logging::logger('testpro').info "db creation response" + db.to_s
          #sleep 30
          #db_detail = JSON.parse(RestClient.get(dburl, {authorization: token, content_type: :json, accept: :json}).body)
          render json: {status: 'SUCCESS', message: 'SQL database created successfully', response: db.to_s, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok

        rescue Exception => e
          render json: {status: 'ERROR', message: "SQL database not created due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end

      end

      def destroy
        data = params.permit(:resource_group, :server_name, :db_name, :delete_server, :compute_resource_id)
        begin

          get_azure_details(data[:compute_resource_id])
          @connector = AzureConnector.new(@client_id, @client_secret, @tenant_id, @subscription_id)
          if (data[:delete_server] == "true")
            delete_response = delete_server data

          else
            delete_response = delete_db data
          end
          ::Foreman::Logging::logger('testpro').info delete_response
          render json: {status: 'SUCCESS', message: 'Delete Operation successfull', response: delete_response.to_json}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "Delete Operation not completed due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def delete_server(data)
        token = @connector.get_token
        url = "https://management.azure.com/subscriptions/#{@connector.subscriptionId}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Sql/servers/#{data[:server_name]}?api-version=2015-05-01-preview"
        return RestClient.delete(url, {authorization: token, content_type: :json, accept: :json})
      end

      def delete_db(data)
        token = @connector.get_token
        url = "https://management.azure.com/subscriptions/#{@connector.subscriptionId}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Sql/servers/#{data[:server_name]}/databases/#{data[:db_name]}?api-version=2017-10-01-preview"
        return RestClient.delete(url, {authorization: token, content_type: :json, accept: :json})
      end

      def create_server(data)
        token = @connector.get_token
        url = "https://management.azure.com/subscriptions/#{@connector.subscriptionId}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Sql/servers/#{data[:server_name]}?api-version=2015-05-01-preview"
        payload = {
            properties: {
                administratorLogin: data[:administrator_login],
                administratorLoginPassword: data[:administrator_login_password],
                "version": data[:version],
                "state": "Ready"
            },
            location: data[:location] #"australiaeast"
        }

        resp = RestClient.put(
            url,
            JSON.generate(payload),
            {authorization: token, content_type: :json, accept: :json}
        )
        return JSON.parse(resp.body)
      end

      def create_db(data)
        token = @connector.get_token
        dburl = "https://management.azure.com/subscriptions/#{@connector.subscriptionId}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Sql/servers/#{data[:server_name]}/databases/#{data[:db_name]}?api-version=2017-10-01-preview"
        dbpayload = {
            "sku": {
                "name": "S0"
            },
            "properties": {
                "maxSizeBytes": 0,
                "readScale": "Disabled",
                "collation": data[:collation],
                "createMode": data[:create_mode]
            },
            "location": data[:location]
        }
        resp = RestClient.put(
            dburl,
            JSON.generate(dbpayload),
            {authorization: token, content_type: :json, accept: :json}
        )
        return JSON.parse(resp.body)
      end


    end
  end
end
