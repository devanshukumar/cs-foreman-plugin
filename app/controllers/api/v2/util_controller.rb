module Api
  module V2
    class UtilController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def set_cr_value
        begin
          crs = ComputeResource.where type: 'Foreman::Model::EC2'

          crs.each do |cr|
            cr.user = params["user"]
            cr.password = params["password"]
            cr.save
            ::Foreman::Logging::logger('testpro').info cr.user
          end
          render json: {status: 'SUCCESS', message: 'Util Run completed'}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While Util run #{e.message}"}, status: :unprocessable_entity
        end
      end
    end
  end
end


