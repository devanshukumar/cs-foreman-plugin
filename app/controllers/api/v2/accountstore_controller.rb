require 'fog/azurerm'

module Api
  module V2
    class StorageaccountController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      # this function will execute in the case of post call
      def create
        data = params.permit(:tenant_id, :client_id, :client_secret, :subscription_id, :environment,
                             :storage_account_name, :location, :resource_group, :account_type, :replication, :encryption, :end_point_url, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection(data)

          if (responsePayload = fog_storage_service.storage_accounts.check_storage_account_exists(data[:resource_group], data[:storage_account_name]))

            render json: {status: 'SUCCESS', message: 'Storage Account Name Already Exist', Response: responsePayload}, status: :ok
          else
            accountPayoadResp = create_storage_account(fog_storage_service, data)
            render json: {status: 'SUCCESS', message: 'Storage Account Created Successfully', Response: accountPayoadResp}, status: :ok

          end
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Storage Account not Created due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will execute in the case of delete call and destroy the storage account 
      def destroy
        data = data = params.permit(:tenant_id, :client_id, :client_secret, :subscription_id, :environment,
                                    :storage_account_name, :resource_group, :end_point_url, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection(data)
          storage_acc = fog_storage_service.storage_accounts.get(data[:resource_group], data[:storage_account_name])
          account_nm = storage_acc.name
          if (resp = storage_acc.destroy)
            render json: {status: 'SUCCESS', message: "Storage Account Destroy", Response: account_nm}, status: :ok
          end
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem while Storage Account Deleting due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will create the connection with existing storage account
      private def create_connection(data)
        cr = ComputeResource.find(data[:compute_resource_id])
        fog_storage_service = Fog::Storage::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_storage_service
      end
      #this function will create the storage account 
      private def create_storage_account(fog_storage_service, data)

        accountPayloadResp = fog_storage_service.storage_accounts.create(
            name: data[:storage_account_name],
            location: data[:location],
            resource_group: data[:resource_group],
            account_type: data[:account_type],
            replication: data[:replication],
            encryption: data[:encryption]
        )
        return accountPayloadResp
      end
    end
  end
end

