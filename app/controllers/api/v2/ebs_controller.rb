require 'aws-sdk-elasticbeanstalk' # v2: require 'aws-sdk'

module Api
  module V2
    class EbsController < ApplicationController
      skip_before_action :verify_authenticity_token


      def create

        begin
          data = params.permit(:app_desc, :application_name, :solution_stack_name, :version_label, :environment_name,
                               :auto_create_application, :ver_desc, :process, :s3_bucket, :s3_key, :cname_prefix, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          elb = Aws::ElasticBeanstalk::Client.new(:access_key_id => cr.user,
                                                  :secret_access_key => cr.password, :region => cr.url)
          respEBSApp = elb.create_application({application_name: data[:application_name], description: data[:app_desc]})
          ::Foreman::Logging::logger('testpro').info respEBSApp
          respEBSVer = createEBSVersion(data[:application_name], data[:auto_create_application], data[:ver_desc], data[:process], data[:s3_bucket], data[:s3_key], data[:version_label], data[:compute_resource_id])

          # respEBSEnv = elb.create_environment({application_name: data[:application_name],
          #                                      environment_name: data[:environment_name],
          #                                      solution_stack_name: data[:version_label],
          #                                      tags: [{key: "Name",value: data[:application_name],},]})

          #::Foreman::Logging::logger('testpro').info respEBSEnv
          sleep 20
          respEBSEnv = elb.create_environment({application_name: data[:application_name],
                                               environment_name: data[:environment_name],
                                               solution_stack_name: data[:version_label], version_label: data[:version_label],
                                               tags: [{key: "Name", value: data[:application_name], },]})
          ::Foreman::Logging::logger('testpro').info respEBSEnv

          ::Foreman::Logging::logger('testpro').info respEBSVer
          render json: {status: 'SUCCESS', message: 'EBSApplication Created', response: {application_reponse: respEBSApp.to_hash, environment_response: respEBSVer.to_hash}}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'EBS application not created', data: "#{e}"}, status: :'Exception in Creating EBS Application'
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def destroy

        begin
          data = params.permit(:application_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          elb = Aws::ElasticBeanstalk::Client.new(:access_key_id => cr.user,
                                                  :secret_access_key => cr.password, :region => cr.url)
          respDEBSApp = elb.delete_application({application_name: data[:application_name], terminate_env_by_force: true})
          ::Foreman::Logging::logger('testpro').info respDEBSApp
          render json: {status: 'SUCCESS', message: 'EBSApplication Deleted', response: {application_reponse: {}}}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'EBS application not deleted', data: "#{e}"}, status: :'Exception in Deleting EBS Application'
          ::Foreman::Logging::logger('testpro').error e
        end
      end


      def deleteEBSApplication

        begin
          data = params.permit(:application_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          client = Aws::ElasticBeanstalk::Client.new(:access_key_id => cr.user,
                                                     :secret_access_key => cr.password, :region => cr.url)

          resp = client.delete_application({
                                               application_name: data[:application_name],
                                               terminate_env_by_force: true
                                           })

          render json: {status: 'SUCCESS', message: 'EBSApplication Deleted', responseEBSApp: "#{resp}"}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'EBS application not deleted', data: "#{e}"}, status: :'Exception in Deleting
                          EBS Application'
          ::Foreman::Logging::logger('testpro').error e
        end
      end


      def createEBSVersion(application_name, auto_create_application, description, process, s3_bucket, s3_key, version_label, compute_resource_id)

        cr = ComputeResource.find(compute_resource_id)
        client = Aws::ElasticBeanstalk::Client.new(:access_key_id => cr.user,
                                                   :secret_access_key => cr.password, :region => cr.url)
        resp = client.create_application_version({
                                                     application_name: application_name,
                                                     auto_create_application: auto_create_application,
                                                     description: description,
                                                     process: true, source_bundle: {
                s3_bucket: s3_bucket,
                s3_key: s3_key,
            },
                                                     version_label: version_label,
                                                 })
        return resp

      end

      def get_solution_stacks

        begin
          cr = ComputeResource.find_by type: 'Foreman::Model::EC2'
          client = Aws::ElasticBeanstalk::Client.new(:access_key_id => cr.user, :secret_access_key => cr.password, :region => cr.url)
          response = client.list_available_solution_stacks({
                                                           })
          ::Foreman::Logging::logger('testpro').info response
          render json: {status: 'SUCCESS', message: 'Solution stack  Listing', response: response.solution_stacks}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'FAILURE', message: 'Error in Solution stack  Listing', response: "#{e}"}
        end

      end

    end
  end
end



