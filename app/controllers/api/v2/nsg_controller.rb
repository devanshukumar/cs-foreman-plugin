require 'fog/azurerm'

module Api
  module V2
    class NsgController < ApplicationController
      skip_before_action :verify_authenticity_token

      def list
        begin
          cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
          fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')

          network_security_groups = fog_network_service.network_security_groups(resource_group: params[:id])
          @nsgl = []
          network_security_groups.each do |nsg|
            nsg.security_rules.each do |nsgi|
              @nsgl << {'id': nsgi.id, 'name': nsgi.name}
            end
          end
          render json: {status: 'SUCCESS', nsg_list: @nsgl}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'NSG Listing1', data: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end
