require 'fog/azurerm'

module Api
  module V2
    class SubnetapiController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def showSubnets
        subnetName = Array.new

        begin
          fog_network_service = create_connection()

          subnets = fog_network_service.subnets(resource_group: params[:resource_group], virtual_network_name: params[:id])
          if (params.has_key?(:get_id) && params[:get_id])
            subnets.each do |subnet|
              subnetName << {subnet.name => subnet.id}
            end
          else
            subnets.each do |subnet|
              subnetName.push(subnet.name)
            end
          end
          render json: {status: 'SUCCESS', message: 'List of subnet', Response: subnetName}, status: :ok


        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While subnet due to #{e.message}"}, status: :unprocessable_entity
        end

      end

      #this function will create the connection with existing storage account
      private def create_connection()
        cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
        fog_network_service = Fog::Network::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_network_service
      end
    end
  end
end
