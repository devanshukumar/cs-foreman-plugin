require 'fog/azurerm'

module Api
  module V2
    class ResourcegroupnameController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)
        cr = ComputeResource.find(id);
        return cr
      end

      def index

        resourceName = Array.new
        data = params.permit(:tenant_id, :client_id, :client_secret, :subscription_id,
                             :environment, :end_point_url, :compute_resource_id, :organization_id)
        begin
          fog_storage_service = create_connection()
          fog_storage_service.resource_groups.each do |resource_group|
            resourceName.push(resource_group.name)
          end
          render json: {status: 'SUCCESS', message: 'Resource Group', Response: resourceName}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While Iteration Resource Group due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will create the connection with existing storage account
      private def create_connection()
        cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
        fog_storage_service = Fog::Resources::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_storage_service
      end
    end
  end
end

