require 'fog/azurerm'

module Api
  module V2
    class AzureController < ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)
        cr = ComputeResource.find(id);
        return cr
      end

      def deleteExpressRoute
        begin
          data = params.permit(:resource_group, :express_route_circuit_name, :compute_resource_id)
          cr = getComputeResourceValue(data[:compute_resource_id])
          fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')

          circuit = fog_network_service.express_route_circuits.get(data[:resource_group], data[:express_route_circuit_name])
          circuit.destroy
          ::Foreman::Logging::logger('testpro').info "#{circuit.name}"
          render json: {status: 'SUCCESS', message: 'Expressroute Deleted'}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in deletion in Expressroute', data: "#{e}"}, status: :unprocessable_entity

        end
      end


      def expressRouteCircuit
        begin
          data = params.permit(:express_route_circuit_name, :location, :resource_group, :sku_tier, :sku_name, :sku_family,
                               :service_provider_name, :peering_location, :bandwidth_in_mbps, :compute_resource_id)

          cr = getComputeResourceValue(data[:compute_resource_id])
          fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')
          resp = fog_network_service.express_route_circuits.create(
              name: data[:express_route_circuit_name],
              location: data[:location],
              resource_group: data[:resource_group],
              sku_name: data[:sku_name],
              sku_tier: data[:sku_tier],
              sku_family: data[:sku_family],
              service_provider_name: data[:service_provider_name],
              peering_location: data[:peering_location],
              bandwidth_in_mbps: data[:bandwidth_in_mbps],
              tags: {key: 'value'})

          render json: {status: 'SUCCESS', message: 'Express Route Circuit Created', response: resp.to_json}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in creation in Express Route Circuit', response: "#{e}"}, status: :unprocessable_entity
        end
      end

      def createVirtualNetworkGateway
        begin
          cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
          data = params.permit(:resource_group, :location, :gateway_name, :disk_size_in_gb, :ip_config_name, :location,
                               :private_ipallocation_method, :subnet_id, :sku_tier, :sku_name, :sku_capacity, :gateway_type, :enable_bgp, :gateway_size, :asn, :bgp_peering_address, :vpn_type, :gateway_default_site, :ip_name, :public_ip_allocation_method)

          fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')

          public_ip = fog_network_service.public_ips.create(
              name: data[:ip_name],
              resource_group: data[:resource_group],
              location: data[:location],
              public_ip_allocation_method: data[:public_ip_allocation_method])

          resp = fog_network_service.virtual_network_gateways.create(
              name: data[:gateway_name],
              location: data[:locatION],
              ip_configurations: [
                  {
                      name: data[:ip_config_name], #'abc',
                      private_ipallocation_method: data[:private_ipallocation_method], #'Dynamic',
                      public_ipaddress_id: public_ip.id, #'/subscriptions/438ce04d-6c98-4124-bec2-af86e80fa980/resourceGroups/Testingruby/providers/Microsoft.Network/Create new',
                      subnet_id: data[:subnet_id], #'/subscriptions/438ce04d-6c98-4124-bec2-af86e80fa980/resourceGroups/Testingruby /providers/Microsoft.Network/virtualNetworks/<Virtual Network Name>/Ruby1',
                      private_ipaddress: 'nil', # could be 'nil'
                  }
              ],
              resource_group: data[:resource_group], #'CFXtest',
              sku_tier: data[:sku_tier], #'VpnGw1',
              sku_name: data[:sku_name], #'VpnGw1',
              sku_capacity: data[:sku_capacity], #'Dynamic',
              gateway_type: data[:gateway_type], #'ExpressRoute',
              enable_bgp: data[:enable_bgp], #'False',
              gateway_size: data[:gateway_size], #'Basic',
              asn: data[:asn], #65010,
              bgp_peering_address: data[:bgp_peering_address], #'10.12.255.30',
              peer_weight: ' ',
              vpn_type: data[:vpn_type], #'RouteBased',
          )

          render json: {status: 'SUCCESS', message: 'VPNGateway Created', response: public_ip.to_json}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in creation in VPNGateway and PublicIP', data: e.to_json, response: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def createPublicIp
        data = params.permit(:ip_name, :resource_group, :location, :public_ip_allocation_method)
        cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
        fog_network_service = Fog::Network::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud')

        public_ip = fog_network_service.public_ips.create(
            name: data[:ip_name],
            resource_group: data[:resource_group],
            location: data[:location],
            public_ip_allocation_method: data[:public_ip_allocation_method])
      end
    end
  end
end


