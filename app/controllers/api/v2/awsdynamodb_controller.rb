require 'aws-sdk-dynamodb'

module Api
  module V2
    class AwsdynamodbController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def create
        data = params.permit(:table_name, :primary_key, :compute_resource_id)

        begin
          fog_service_connection = create_connection(data)

          resp = fog_service_connection.create_table({
                                                         table_name: data[:table_name],
                                                         attribute_definitions: [
                                                             {attribute_name: data[:primary_key], attribute_type: 'S'},
                                                             {attribute_name: 'product_id', attribute_type: 'S'}
                                                         ],
                                                         key_schema: [
                                                             {attribute_name: data[:primary_key], key_type: 'HASH'},
                                                             {attribute_name: 'product_id', key_type: 'RANGE'}
                                                         ],
                                                         provisioned_throughput: {
                                                             read_capacity_units: 1,
                                                             write_capacity_units: 1
                                                         }
                                                     })
          tarn = resp[:table_description][:table_arn]
          ::Foreman::Logging::logger('testpro').info tarn
          sleep 15
          fog_service_connection.tag_resource({resource_arn: tarn, tags: [{key: "Name", value: tarn, },], })

          render json: {status: 'SUCCESS', message: 'DynamoDB table Created Successfully', response: "#{resp.data.to_hash}"}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "DynamoDB table not Created due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      def destroy
        data = data = params.permit(:table_name, :compute_resource_id)
        params = {
            table_name: data[:table_name]
        }

        begin
          fog_storage_service = create_connection(data)
          result = fog_storage_service.delete_table(params)
          render json: {status: 'SUCCESS', message: "DynamoDB Destroy", response: result.data.to_hash}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem while Deleting DynamoDB due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will create the connection with existing storage account
      private def create_connection(data)
        cr = cr = ComputeResource.find(data[:compute_resource_id])
        fog_service_connection = Aws::DynamoDB::Client.new(:access_key_id => cr.user,
                                                           :secret_access_key => cr.password,
                                                           :region => cr.url)
        return fog_service_connection
      end
    end
  end
end
