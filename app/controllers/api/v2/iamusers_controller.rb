require 'aws-sdk-iam' # v2: require 'aws-sdk'

module Api
  module V2
    class IamusersController < ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)
        cr = ComputeResource.find(id)
        return Aws::IAM::Client.new(:access_key_id => cr.user,
                                    :secret_access_key => cr.password, :region => cr.url)
      end

      def create
        begin
          username = params[:username]
          password = params[:password]
          iam_client = getComputeResourceValue(params[:compute_resource_id])
          response = {}
          user_response = iam_client.create_user({
                                                     user_name: username,
                                                 })
          response["user_response"] = user_response.to_hash
          iam_client.wait_until(:user_exists, user_name: username)
          if params["accessKey"] == "true"
            accesskey_response = iam_client.create_access_key({
                                                                  user_name: username,
                                                              })
            response["accesskey_response"] = accesskey_response.to_hash
            ::Foreman::Logging::logger('testpro').info accesskey_response
          end
          if params["login"] == "true"
            loginprofile_response = iam_client.create_login_profile({password: password, user_name: username})
            response["loginprofile_response"] = loginprofile_response.login_profile
            ::Foreman::Logging::logger('testpro').info loginprofile_response
          end
          if params.has_key? "policies"
            params["policies"].each do
            |policy|
              resp = iam_client.attach_user_policy({
                                                       policy_arn: policy,
                                                       user_name: username
                                                   })
            end
          end

          if params.has_key? "group"
            group_addition = iam_client.add_user_to_group({
                                                              group_name: params[:group],
                                                              user_name: params[:username],
                                                          })
            ::Foreman::Logging::logger('testpro').info group_addition
          end


          render json: {status: 'SUCCESS', message: 'IAM user Created', response: response}, status: :ok
        rescue Aws::IAM::Errors::EntityAlreadyExists
          render json: {status: 'ERROR', message: "User already exists"}, status: :unprocessable_entity
        end

      rescue Exception => e
        render json: {status: 'ERROR', message: "IAM user faced problems due to #{e}"}, status: :unprocessable_entity
        ::Foreman::Logging::logger('testpro').error e
      end

      def destroy
        begin
          username = params[:id]
          reponse = {}
          iam_client = getComputeResourceValue(params[:compute_resource_id])
          #accesskey_deletion_response = iam_client.delete_access_key(user_name: params[:id])
          begin

            login = iam_client.get_login_profile({
                                                     user_name: params[:id],
                                                 })
            login_profile_deletion_response = iam_client.delete_login_profile(user_name: params[:id])
            ::Foreman::Logging::logger('testpro').info login_profile_deletion_response

          rescue Aws::IAM::Errors::NoSuchEntity
            ::Foreman::Logging::logger('testpro').info "No login profile found"
          end


          accesskeys = iam_client.list_access_keys({
                                                       user_name: params[:id],
                                                   })

          unless accesskeys.access_key_metadata.count == 0
            accesskeys.access_key_metadata.each {|accesskey|
              iam_client.delete_access_key({
                                               access_key_id: accesskey.access_key_id,
                                               user_name: accesskey.user_name,
                                           })
            }
          end
          user_policies = iam_client.list_attached_user_policies({
                                                                     user_name: params[:id]
                                                                 })

          user_policies[0].each {|user_policy| response = iam_client.detach_user_policy({
                                                                                            user_name: params[:id], # required
                                                                                            policy_arn: user_policy.policy_arn, # required
                                                                                        })
          ::Foreman::Logging::logger('testpro').info response
          }
          user_groups = iam_client.list_groups_for_user({
                                                            user_name: params[:id],
                                                        })

          user_groups[0].each do

          |user_group|
            ::Foreman::Logging::logger('testpro').info "removing user from #{user_group.group_name}"
            resp = iam_client.remove_user_from_group({
                                                         group_name: user_group.group_name,
                                                         user_name: params[:id],
                                                     })

          end
          response = iam_client.delete_user({
                                                user_name: params[:id],
                                            })

          render json: {status: 'SUCCESS', message: 'IAM user deleted'
          }, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "IAM user not deleted due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end

  end
end
