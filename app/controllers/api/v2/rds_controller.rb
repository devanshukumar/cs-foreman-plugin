require 'aws-sdk'

module Api
  module V2
    class RdsController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)

        cr = ComputeResource.find(id);
        return cr

      end

      def index

        begin
          render json: {status: 'SUCCESS', message: 'Loaded CloudDatabase', data: {}}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'RDS not saved', data: e}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end

      end

      def create

        begin
          data = params.permit(:db_name, :allocated_storage, :db_instance_class, :engine, :master_username, :master_user_password,
                               :storage_type, :db_id, :compute_resource_id)
          resp = createRDSInstance(data[:db_name], data[:allocated_storage], data[:db_instance_class], data[:engine], data[:master_username], data[:master_user_password], data[:storage_type], data[:db_id], data[:compute_resource_id])
          ::Foreman::Logging::logger('testpro').info resp.db_name

          resp.data.engine = "AWS:RDS:#{resp.data.engine}"
          render json: {status: 'SUCCESS', message: 'Saved CloudDatabase', response: resp.data, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'RDS not created', data: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def deleteRDS

        begin
          data = params.permit(:skip_final_snapshot, :final_db_snapshot_identifier, :db_id, :compute_resource_id)
          resp = deleteRDSInstance(data[:skip_final_snapshot], data[:final_db_snapshot_identifier], data[:db_id], data[:compute_resource_id])
          render json: {status: 'SUCCESS', message: 'Deleting CloudDatabase', data: resp.data}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'RDS not deleted', data: "#{e}"}, status: :'Exception in Deleting RDS'
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def createRDSInstance(db_name, allocated_storage, db_instance_class, engine, master_username, master_user_password, storage_type, db_id, compute_resource_id)
        cr = ComputeResource.find(compute_resource_id)

        rds = Aws::RDS::DBInstance.new(:access_key_id => cr.user,
                                       :secret_access_key => cr.password, :region => cr.url, :id => db_id)
        rdsInstance = rds.create({
                                     db_name: db_name, # "test_anupama_rds",
                                     allocated_storage: allocated_storage, #1000,
                                     db_instance_class: db_instance_class, #"db.m1.small", # required
                                     engine: engine, #"MySQL", # required
                                     master_username: master_username, #"test_anupama_2",
                                     master_user_password: master_user_password, #"test12345",
                                     storage_type: storage_type, #"standard",
                                 })
        #billing
        rds_client = Aws::RDS::Client.new(:access_key_id => cr.user,
                                          :secret_access_key => cr.password, :region => cr.url)
        tag_response = rds_client.add_tags_to_resource({
                                                           resource_name: rdsInstance.data.db_instance_arn,
                                                           tags: [
                                                               {
                                                                   key: "Name",
                                                                   value: rdsInstance.data.db_instance_arn,
                                                               },
                                                           ],
                                                       })
        ::Foreman::Logging::logger('testpro').info tag_response
        return rdsInstance
      end

      def deleteRDSInstance(skip_final_snapshot, final_db_snapshot_identifier, db_id, compute_resource_id)
        cr = ComputeResource.find(compute_resource_id)
        rds = Aws::RDS::DBInstance.new(:access_key_id => cr.user,
                                       :secret_access_key => cr.password, :region => cr.url, :id => db_id)

        dbinstance = rds.delete({
                                    skip_final_snapshot: skip_final_snapshot, #false,
                                    final_db_snapshot_identifier: final_db_snapshot_identifier, #"mydb",
                                })

      end


      #create snapshot
      def createSnapshot
        begin
          data = params.permit(:source_db_name, :target_db_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          rds = Aws::RDS::DBInstance.new(:access_key_id => cr.user,
                                         :secret_access_key => cr.password,
                                         :region => cr.url,
                                         :id => data[:source_db_name])
          rds.create_snapshot({db_snapshot_identifier: data[:target_db_name]})
          render json: {status: 'SUCCESS', message: 'snapshot created'}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: 'snapshot not created', data: "#{e}"}, status: :unprocessable_entity
        end
      end

      #restore Latest Time db
      def restoreLatestSnapshot
        begin
          #dbInstance client
          data = params.permit(:source_db_name, :target_db_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          rds = Aws::RDS::DBInstance.new(:access_key_id => cr.user,
                                         :secret_access_key => cr.password,
                                         :region => cr.url,
                                         :id => data[:source_db_name])
          latestRT = rds.latest_restorable_time
          ::Foreman::Logging::logger('testpro').info latestRT
          resp = rds.restore({target_db_instance_identifier: data["target_db_name"], restore_time: latestRT})
          render json: {status: 'SUCCESS', message: 'snapshot created'}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: 'snapshot not created', data: "#{e}"}, status: :unprocessable_entity
        end
      end

      #restore db
      def restoreSnapshot
        begin
          #dbInstance client
          data = params.permit(:source_db_name, :target_db_name, :source_snapshot_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          rds = Aws::RDS::DBSnapshot.new(:access_key_id => cr.user,
                                         :secret_access_key => cr.password,
                                         :region => cr.url,
                                         :instance_id => data[:source_db_name], :snapshot_id => data[:source_snapshot_name])
          resp = rds.restore({db_instance_identifier: data[:target_db_name]})
          render json: {status: 'SUCCESS', message: 'instance restored from snapshot'}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: 'instance not restored from snapshot', data: "#{e}"}, status: :unprocessable_entity
        end
      end

      def listSnapshot
        begin
          cr = ComputeResource.find(params[:id])
          rds = Aws::RDS::Resource.new(:access_key_id => cr.user,
                                       :secret_access_key => cr.password,
                                       :region => cr.url)
          db_list = []
          rds.db_instances.each do |i|
            puts "Name (ID): #{i.id}"
            snapshot_list = []
            i.snapshots.each do |s|
              snapshot_list.push("#{s.snapshot_id}")
            end
            db_list.push({"#{i.id}": snapshot_list})
          end
          render json: {status: 'SUCCESS', data: db_list}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', data: "#{e}"}, status: :unprocessable_entity
        end
      end

    end
  end
end



