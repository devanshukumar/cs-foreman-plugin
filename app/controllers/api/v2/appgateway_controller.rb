require 'rest-client'
require 'fog/azurerm'
require 'json'
require_relative 'azure_properties'
module Api
  module V2
    class AppgatewayController < ::ApplicationController

      skip_before_action :verify_authenticity_token

      def get_azure_details(id)
        cr = ComputeResource.find(id)
        @tenant_id = cr.uuid
        @subscription_id = cr.user
        @client_id = cr.url
        @client_secret = cr.password
        #####
      end


      def destroy
        begin
          data = params.permit(:resource_group, :application_name, :compute_resource_id)
          get_azure_details(data[:compute_resource_id])

          network = Fog::Network::AzureRM.new(tenant_id: @tenant_id,
                                              client_id: @client_id,
                                              client_secret: @client_secret,
                                              subscription_id: @subscription_id)

          fog_application_gateway_service = Fog::ApplicationGateway::AzureRM.new(
              tenant_id: @tenant_id,
              client_id: @client_id,
              client_secret: @client_secret,
              subscription_id: @subscription_id,
              environment: 'AzureCloud')
          gateway = fog_application_gateway_service
                        .gateways
                        .get(data[:resource_group], data[:application_name])
          gateway.destroy

          begin
            gateway.frontend_ip_configurations.each do |pip|
              ::Foreman::Logging::logger('testpro').info pip.public_ip_address_id.split("/")[-1]
              pubip = network.public_ips.get(data[:resource_group], pip.public_ip_address_id.split("/")[-1])
              pubip.destroy
            end
            ::Foreman::Logging::logger('testpro').info "public ip destroyed"
          rescue
            ::Foreman::Logging::logger('testpro').error "public ip not destroyed"
          end

          render json: {status: 'SUCCESS', message: 'app gateway deleted successfully'}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "Appgateway not deleted due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def create

        data = params.permit(:resource_group, :application_name, :vnet_name, :location, :agsubnet, :sku_name, :sku_tier, :sku_capacity, :compute_resource_id)
        get_azure_details(data[:compute_resource_id])
        tenant_id = @tenant_id,
            client_id = @client_id,
            client_secret = @client_secret,
            subscription_id = @subscription_id
        res_group = data[:resource_group]
        appname = data[:application_name]
        vnet = data[:vnet_name]
        location = data[:location]
        pip = "#{appname}-PIP"
        agsubnet = data[:agsubnet]
        options = {sku_name: data[:sku_name],
                   sku_tier: data[:sku_tier],
                   sku_capacity: data[:sku_capacity]}


        begin
          connector = AzureConnector.new(@client_id, @client_secret, @tenant_id, @subscription_id)
          token = connector.get_token


          public_ip = connector.get_network_connector.public_ips.create(
              name: "#{pip}",
              resource_group: "#{res_group}",
              location: "#{location}",
              public_ip_allocation_method: 'Dynamic',
          )

          ::Foreman::Logging::logger('testpro').info public_ip
          url = "https://management.azure.com/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/applicationGateways/#{appname}?api-version=2018-04-01"
          payload = connector.get_appgateway_payload(appname, res_group, pip, agsubnet, vnet, location, subscription_id, options)
          resp = RestClient.put(
              url,
              JSON.generate(payload),
              {authorization: token, content_type: :json, accept: :json}
          )
          server = JSON.parse(resp.body)
          ::Foreman::Logging::logger('testpro').info server
          if resp.code != 201
            raise
          end
          render json: {status: 'SUCCESS', code: resp.code, message: 'app gateway created successfully', response: server}, status: :ok
        rescue Exception => e
          begin
            public_ip = connector.get_network_connector.public_ips.get(res_group, pip)
            public_ip.destroy
            ::Foreman::Logging::logger('testpro').info "public IP destroyed"
          rescue
            ::Foreman::Logging::logger('testpro').info "public IP not destroyed"
          end
          render json: {status: 'ERROR', message: "Appgateway not created due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end

      end
    end
  end
end
