require 'fog/azurerm'

module Api
  module V2
    class StorageaccountlistController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def index
        accountNm = Array.new

        data = params.permit(:tenant_id, :client_id, :resource_group_name, :client_secret, :subscription_id,
                             :environment, :end_point_url, :compute_resource_id, :organization_id)
        begin
          fog_storage_service = create_connection()
          storage_accounts = fog_storage_service.storage_accounts(resource_group: data[:resource_group_name])
          storage_accounts.each do |storage_acc|
            accountNm.push(storage_acc.name)
          end
          render json: {status: 'SUCCESS', message: 'List of Storage Account', Response: accountNm}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While Iterating Storage Account due to #{e.message}"}, status: :unprocessable_entity
        end

      end

      #this function will create the connection with existing storage account
      private def create_connection()
        cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
        fog_storage_service = Fog::Storage::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_storage_service
      end

    end
  end
end

