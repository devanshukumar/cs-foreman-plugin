require 'azure/storage'
require 'fog/azurerm'
require 'azure/storage/file'
require "azure/storage/common"

module Api
  module V2
    class FilestoreController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def index
        filestore = FileStore.order("created_at DESC")
        render json: {status: 'SUCCESS', message: 'Successfully Loaded All Record', data: filestore}, status: :ok
      end

      def destroy

        data = params.permit(:storage_account_name, :resource_group,
                             :file_share_name, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection(data)
          storage_acc = fog_storage_service.storage_accounts.get(data[:resource_group], data[:storage_account_name])
          keys_hash = storage_acc.get_access_keys
          storage_access_key = keys_hash[0].value
          filestore = Azure::Storage::File::FileService.create(:storage_account_name => data[:storage_account_name], :storage_access_key => storage_access_key)
          filestore.delete_share(data[:file_share_name])
          render json: {status: 'SUCCESS', message: 'File Share Store Deleted Successfully', Response: data}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "File Share Storage not Deleted due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      def create

        data = params.permit(:storage_account_name, :resource_group,
                             :file_share_name, :quota, :end_point_url, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection(data)
          storage_acc = fog_storage_service.storage_accounts.get(data[:resource_group], data[:storage_account_name])
          keys_hash = storage_acc.get_access_keys
          storage_access_key = keys_hash[0].value
          filestore = Azure::Storage::File::FileService.create(:storage_account_name => data[:storage_account_name], :storage_access_key => storage_access_key)
          filestore.create_share(data[:file_share_name])
          render json: {status: 'SUCCESS', message: 'File Share Store Created Successfully', Response: data}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "File Share Storage not Created due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will create the connection with existing storage account
      private def create_connection(data)

        cr = ComputeResource.find(data[:compute_resource_id])
        fog_storage_service = Fog::Storage::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_storage_service
      end
    end
  end
end

