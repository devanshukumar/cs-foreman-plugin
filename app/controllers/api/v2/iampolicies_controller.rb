require 'aws-sdk-iam'

module Api
  module V2
    class IampoliciesController < ApplicationController
      skip_before_action :verify_authenticity_token

      def index
        begin
          cr = ComputeResource.find_by type: 'Foreman::Model::EC2'
          @iam_client = Aws::IAM::Client.new(:access_key_id => cr.user,
                                             :secret_access_key => cr.password, :region => cr.url)

          policy_names = []
          policy_arns = []
          policies = @iam_client.list_policies({})
          policies[0].each do |policies|
            policy_names << policies.policy_name
            policy_arns << policies.arn

          end
          render json: {status: 'SUCCESS', message: 'IAM policies fetched',
                        response: {policy_names: policy_names, policy_arns: policy_arns}
          }, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "IAM policies not fetched due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end
