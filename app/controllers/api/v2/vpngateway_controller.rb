require 'fog/azurerm'
require 'kafka'


module Api
  module V2
    class VpngatewayController < ApplicationController
      skip_before_action :verify_authenticity_token
##make sure langateway is in the same RG and location,subnet must be named 'GatewaySubnet' for testing
      def getComputeResourceValue(id)
        cr = ComputeResource.find(id);
        return cr
      end


      def destroy
        begin
          data = params.permit(:resource_group, :gateway_name,:compute_resource_id)

          concurrent_obj = VpnGateway.new
	  #removing yaml config as we are using /etc/apache2/envvars updation
#          kconf = YAML.load File.read("/testpro/config/production.yml")
#          ::Foreman::Logging::logger('testpro').info kconf
          data[:BOOTSTRAP_SERVERS]= ENV["BOOTSTRAP_SERVERS"]
	  ::Foreman::Logging::logger('testpro').info "ENV value for bootstrap #{ENV['BOOTSTRAP_SERVERS']}"
          kafka = Kafka.new([data['BOOTSTRAP_SERVERS']])
          kafka.topics # kafka connection test
          concurrent_obj.async.vpnGatewayDeleteJob(kafka, data)
          render json: {status: 'SUCCESS', message: 'VPN Gateway Deletion Request Submitted'}, status: :accepted
        rescue Exception => e
          render json: {status: 'FAILURE', message: "Error Submitting in VPNGateway deletion due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end


      def create
        begin
          data = params.permit(:resource_group, :location, :gateway_name, :disk_size_in_gb, :ip_config_name, :location,
                               :private_ipallocation_method, :subnet_id, :sku_tier, :sku_name, :sku_capacity, :gateway_type, :enable_bgp,
                               :gateway_size, :asn, :bgp_peering_address, :vpn_type, :gateway_default_site, :ip_name,
                               :public_ip_allocation_method,:vpn_device_ip,:compute_resource_id)
          concurrent_obj = VpnGateway.new
          #kconf = YAML.load File.read("/testpro/config/production.yml")
          #::Foreman::Logging::logger('testpro').info kconf
          data[:BOOTSTRAP_SERVERS] = ENV["BOOTSTRAP_SERVERS"] #Rails.application.config.bootstrap_servers
          #template = ERB.new File.new("/testpro/app/controllers/api/v2/production.yml.erb").read
          #processed = YAML.load template.result(binding)
          kafka = Kafka.new([data['BOOTSTRAP_SERVERS']])
          kafka.topics # kafka connection test
	  concurrent_obj.async.vpnGatewayCreateJob(kafka, data)
          render json: {status: 'SUCCESS', message: 'VPN Gateway Creation Request Submitted'}, status: :accepted
        rescue Exception => e
          render json: {status: 'FAILURE', message: "Error Submitting in VPNGateway creation due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end


class VpnGateway
  include Concurrent::Async
 
  def vpnGatewayCreateJob(kafka, data)
    begin
      cr = ComputeResource.find(data[:compute_resource_id]);
      fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')

      public_ip = fog_network_service.public_ips.create(
              name: data[:ip_name],
              resource_group: data[:resource_group],
              location: data[:location],
              public_ip_allocation_method: data[:public_ip_allocation_method])
      ::Foreman::Logging::logger('foreman-tasks').info public_ip
      gateway = fog_network_service.local_network_gateways.create(
        name: "LAN-#{data[:gateway_name]}",
        location: data[:location],
        resource_group: data[:resource_group],
        gateway_ip_address: data[:vpn_device_ip],
        local_network_address_space_prefixes: [],
        asn: data[:asn],
        bgp_peering_address: data[:bgp_peering_address],
        peer_weight: '0',
        tags:   {provisioner: 'CloudSelect-Generated'},)
      ::Foreman::Logging::logger('foreman-tasks').info gateway
      resp = fog_network_service.virtual_network_gateways.create(
              name: data[:gateway_name],
              location: data[:location],
              ip_configurations: [
                  {
                      name: data[:ip_config_name], #'abc',
                      private_ipallocation_method: data[:private_ipallocation_method], #'Dynamic',
                      public_ipaddress_id: public_ip.id, #'/subscriptions/438ce04d-6c98-4124-bec2-af86e80fa980/resourceGroups/Testingruby/providers/Microsoft.Network/Create new',
                      # subnet_id:"/subscriptions/#{cr.url}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Network/virtualNetworks/#{data[:network]}/subnets/#{data[:subnet]}" ,
                      subnet_id: data[:subnet_id],
                              private_ipaddress: 'nil', # could be 'nil'
                  }
              ],
              resource_group: data[:resource_group], #'CFXtest',
              sku_tier: data[:sku_tier], #'VpnGw1',
              sku_name: data[:sku_name], #'VpnGw1',
              sku_capacity: data[:sku_capacity], #'Dynamic',
              gateway_type: data[:gateway_type], #'ExpressRoute',
              enable_bgp: data[:enable_bgp], #'False',
              gateway_size: data[:gateway_size], #'Basic',
              asn: data[:asn], #65010,
              bgp_peering_address: data[:bgp_peering_address], #'10.12.255.30',
              peer_weight: ' ',
              vpn_type: data[:vpn_type], #'RouteBased',
              vpn_client_address_pool: [],
              #gateway_default_site: "/subscriptions/438ce04d-6c98-4124-bec2-af86e80fa980/resourceGroups/appRG/providers/Microsoft.Network/localNetworkGateways/lngateway",
              gateway_default_site: gateway.id, #data[:gateway_default_site],
              default_sites: [])
      ::Foreman::Logging::logger('foreman-tasks').info resp
      kafka.deliver_message({status: 'SUCCESS', message: 'VPN Gateway creation completed', response: resp, request: data}.to_json, topic: "request.create.vpngateway.foreman")
    rescue Exception => e
      ::Foreman::Logging::logger('foreman-tasks').error e
      #cleanup
      #delete public ip
      begin
        pubip = network.public_ips.get(data[:resource_group], data[:ip_name])
        pubip.destroy
        ::Foreman::Logging::logger('foreman-tasks').error "public ip deleted"
      rescue #Exception => e1
      #  ::Foreman::Logging::logger('testpro').error e1
      end
      #delete LAN Gateway
      begin
        fog_network_service.local_network_gateways.delete(data[:resource_group], "LAN-#{data[:gateway_name]}")
        ::Foreman::Logging::logger('foreman-tasks').error "LAN-#{data[:gateway_name]} deleted"
      rescue #Exception => e2
        #::Foreman::Logging::logger('testpro').error e2
      end
      kafka.deliver_message({status: 'FAILURE', message: "Error in creation in VPNGateway due to #{e}", response: e, request: data}.to_json, topic: "request.create.vpngateway.foreman")
    end
  end

  def vpnGatewayDeleteJob(kafka, data)
    begin
          cr = ComputeResource.find(data[:compute_resource_id])
          fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')

          network_gateway = fog_network_service.virtual_network_gateways.get(data[:resource_group], data[:gateway_name])
          resp = network_gateway.destroy

          #delete
          begin
            network_gateway.ip_configurations.each do |nsge|
              pubip = network.public_ips.get(data[:resource_group], nsge.public_ipaddress_id.split("/")[-1])
              pubip.destroy
              ::Foreman::Logging::logger('foreman-tasks').info nsge.public_ipaddress_id.split("/")[-1]
            end
          rescue
          end
          #delete LAN Gateway
          begin
            lg = fog_network_service.local_network_gateways.get(data[:resource_group], "LAN-#{data[:gateway_name]}")
            lg.destroy
            ::Foreman::Logging::logger('foreman-tasks').info network_gateway.gateway_default_site.id.split("/")[-1]
          rescue Exception => e1
            ::Foreman::Logging::logger('foreman-tasks').error e1
          end
 
          kafka.deliver_message({status: 'SUCCESS', message: 'VPN Gateway deletion completed', response: resp, request: data}.to_json, topic: "request.delete.vpngateway.foreman")
    rescue Exception => e
       ::Foreman::Logging::logger('foreman-tasks').error e
       kafka.deliver_message({status: 'FAILURE', message: "Error in deletion in VPNGateway due to #{e}", response: e, request: data}.to_json, topic: "request.delete.vpngateway.foreman")
    end  
  end

end
