require 'aws-sdk-iam' # v2: require 'aws-sdk'

module Api
  module V2
    class IamgroupsController < ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)
        cr = ComputeResource.find(id)
        return Aws::IAM::Client.new(:access_key_id => cr.user,
                                    :secret_access_key => cr.password, :region => cr.url)
      end

      def create
        begin
          iam_client = getComputeResourceValue(params[:compute_resource_id])
          group_response = iam_client.create_group({
                                                       group_name: params[:group],
                                                   })
          if params.has_key? "policies"
            params["policies"].each do
            |policy|
              resp = iam_client.attach_group_policy({
                                                        policy_arn: policy,
                                                        group_name: params[:group]
                                                    })
              ::Foreman::Logging::logger('testpro').info resp
            end
          end
          render json: {status: 'SUCCESS', message: 'IAM group Created', response: group_response.to_hash}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "IAM group not created due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def index
        begin
          iam_client = cr = ComputeResource.find_by type: 'Foreman::Model::EC2'
          groups = []
          iam_client.list_groups[0].each {|group| groups << group.group_name}
          ::Foreman::Logging::logger('testpro').info groups
          render json: {status: 'SUCCESS', message: 'IAM group list', response: groups}, status: :ok

        rescue
          render json: {status: 'ERROR', message: "IAM group listing failed due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end

      end

      def destroy
        begin
          iam_client = getComputeResourceValue(params[:compute_resource_id])
          group = params[:id]
          group_policies = iam_client.list_attached_group_policies({
                                                                       group_name: group
                                                                   })

          group_policies[0].each {|group_policy|
            response = iam_client.detach_group_policy({
                                                          group_name: group, # required
                                                          policy_arn: group_policy.policy_arn, # required
                                                      })
            ::Foreman::Logging::logger('testpro').info response
          }
          response = iam_client.delete_group({
                                                 group_name: group,
                                             })

          render json: {status: 'SUCCESS', message: 'IAM group deleted'
          }, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'IAM group not deleted', data: "#{e}"}, status: :'Exception in Creating EBS Application'
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end
