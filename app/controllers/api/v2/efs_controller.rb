require 'aws-sdk-efs'
module Api
  module V2
    class EfsController < ::ApplicationController

      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)

        cr = ComputeResource.find(id);
        return cr

      end

      def create
        begin
          data = params.permit(:creation_token, :performance_mode, :file_system_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])
          efs = Aws::EFS::Client.new(:access_key_id => cr.user, :secret_access_key => cr.password, :region => cr.url)


          response = efs.create_file_system({creation_token: data[:creation_token], performance_mode: data[:performance_mode]})
          name_tag = efs.create_tags({
                                         file_system_id: response.file_system_id,
                                         tags: [
                                             {
                                                 key: 'Name',
                                                 value: data[:file_system_name],
                                             },
                                         ],
                                     })

          render json: {status: 'SUCCESS', message: 'EFS Created', response: response.to_hash}
        rescue Exception => e
          render json: {status: 'ERROR', message: 'ERROR in Creating EFS', data: "#{e}"}, status: :unprocessable_entity
        end
      end

      def destroy
        cr = ComputeResource.find(params[:compute_resource_id])
        efs = Aws::EFS::Client.new(:access_key_id => cr.user, :secret_access_key => cr.password, :region => cr.url)
        response = efs.delete_file_system({file_system_id: params[:id], })
        render json: {status: 'SUCCESS', message: 'EFS Deleted', response: response.to_hash.to_json}
      rescue Exception => e
        render json: {status: 'ERROR', message: 'ERROR in Deleting EFS', data: "#{e}"}
      end
    end
  end
end

