require 'aws-sdk-sns'
module Api
  module V2
    class SnsController < ::ApplicationController

      skip_before_action :verify_authenticity_token

      def create_connection(id)
        cr = ComputeResource.find(id)
        @client = Aws::SNS::Client.new(:access_key_id => cr.user,
                                       :secret_access_key => cr.password, :region => cr.url)
      end

      def create
        begin
          create_connection(params[:compute_resource_id])
          response = @client.create_topic(name: params[:name])
          ::Foreman::Logging::logger('testpro').info response
          subscription_response = []
          params[:subscriptions].each do
          |subscription|
            sub = @client.subscribe({
                                        topic_arn: response.topic_arn,
                                        protocol: subscription["protocol"],
                                        endpoint: subscription["endpoint"]
                                    })
            ::Foreman::Logging::logger('testpro').info sub
            subscription_response << {protocol: subscription["protocol"],
                                      endpoint: subscription["endpoint"],
                                      subscription_arn: sub.subscription_arn} #.to_hash.to_json

          end


          render json: {status: 'SUCCESS', message: 'SNS created successfully', response: {topic_response: response.topic_arn, subscriptions: subscription_response}, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok

        rescue Exception => e
          render json: {status: 'ERROR', message: "SNS not created due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def index
        begin
          create_connection(params[:compute_resource_id])
          ::Foreman::Logging::logger('testpro').info params
          topics = @client.list_topics
          render json: {status: 'SUCCESS', message: 'SNS listing', response: topics.to_hash, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "SNS not listing due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end

      end

      def destroy
        begin
          create_connection(params[:compute_resource_id])
          response = @client.delete_topic({
                                              topic_arn: params[:arn], # required
                                          })

          ::Foreman::Logging::logger('testpro').info response
          render json: {status: 'SUCCESS', message: 'SNS deleted successfully', response: {}, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "SNS not deleted due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end

  end
end

