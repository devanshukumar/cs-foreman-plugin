require 'fog/azurerm'

module Api
  module V2
    class VnetController < ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)
        cr = ComputeResource.find(id);
        return cr
      end

      def create
        begin
          data = params.permit(:virtual_network_name, :location, :resource_group, :network_security_group_id,
                               :route_table_id, :subnet_name, :subnet_address_prefix, :enable_accelerated_networking,
                               :vnet_address_prefixes, :nsg_name, :rout_table_name, :compute_resource_id)
          cr = getComputeResourceValue(params[:compute_resource_id])
          ::Foreman::Logging::logger('testpro').info "/subscriptions/#{cr.user}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Network/routeTables/#{data[:rout_table_name]}"

          ::Foreman::Logging::logger('testpro').info "/subscriptions/#{cr.user}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Network/networkSecurityGroups/#{data[:nsg_name]}"
          fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')
          subnet_data = [{
                             name: data[:subnet_name], #'default',
                             address_prefix: data[:subnet_address_prefix], # '10.3.0.0/24',

                         }]
          subnet_data.each do |subnet|
            if data[:rout_table_name] != "none"
              subnet[:route_table_id] = "/subscriptions/#{cr.user}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Network/routeTables/#{data[:rout_table_name]}"
            end
            if data[:nsg_name] != "none"
              subnet[:network_security_group_id] = "/subscriptions/#{cr.user}/resourceGroups/#{data[:resource_group]}/providers/Microsoft.Network/networkSecurityGroups/#{data[:nsg_name]}"
            end

          end
          virtualNWResp = fog_network_service.virtual_networks.create(
              name: data[:virtual_network_name],
              location: data[:location],
              resource_group: data[:resource_group],

              route_table_id: data[:route_table_id],
              enable_accelerated_networking: data[:enable_accelerated_networking],
              subnets: subnet_data,
              address_prefixes: [data[:vnet_address_prefixes]], #['10.3.0.0/16'],
              tags: {key: 'value1'},)


          render json: {status: 'SUCCESS', message: 'VNET and Subnet Created', response: virtualNWResp, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'ERROR in VNET and Subnet Creation', data: "#{e}"}
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def destroy
        begin
          data = params.permit(:virtual_network_name, :resource_group, :compute_resource_id)

          cr = getComputeResourceValue(data[:compute_resource_id])
          fog_network_service = Fog::Network::AzureRM.new(
              tenant_id: cr.uuid,
              client_id: cr.url,
              client_secret: cr.password,
              subscription_id: cr.user,
              environment: 'AzureCloud')
          vnet = fog_network_service.virtual_networks.get(data[:resource_group], data[:virtual_network_name])
          ::Foreman::Logging::logger('testpro').info "#{vnet.name}"
          vnet.destroy
          render json: {status: 'SUCCESS', message: 'VNET Deleted'}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: 'ERROR in VNET Deletion', data: "#{e}"}
          ::Foreman::Logging::logger('testpro').error e
        end

      end

    end
  end
end
