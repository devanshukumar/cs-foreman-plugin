require 'fog/azurerm'

module Api
  module V2
    class ContainerstoreController < ::ApplicationController

      skip_before_action :verify_authenticity_token

      def create
        data = params.permit(:storage_account_name, :resource_group_name, :container_name, :access_level, :end_point_url, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection(data)
          storage_acc = fog_storage_service.storage_accounts.get(data[:resource_group_name], data[:storage_account_name])
          keys_hash = storage_acc.get_access_keys
          storage_access_key = keys_hash[0].value
          ::Foreman::Logging::logger('testpro').info keys_hash[0].value
          fog_storage_services = create_connection_with_access(storage_access_key, data)

          if (resp = fog_storage_services.directories.check_container_exists(data[:container_name]))
            render json: {status: 'ERROR', message: 'Container Name  Already Exist', Response: resp}, status: :unprocessable_entity
          else
            directory = fog_storage_services.directories.create(
                key: data[:container_name],
                public: data[:access_level]
            )
            render json: {status: 'SUCCESS', message: 'Container Created Successfully', Response: directory}, status: :ok
          end
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Storage Container not Created due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      def destroy
        data = params.permit(:tenant_id, :client_id, :client_secret, :subscription_id, :storage_account_name, :resource_group_name,
                             :environment, :container_name, :end_point_url, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection(data)
          storage_acc = fog_storage_service.storage_accounts.get(data[:resource_group_name], data[:storage_account_name])
          keys_hash = storage_acc.get_access_keys
          storage_access_key = keys_hash[0].value
          ::Foreman::Logging::logger('testpro').info keys_hash[0].value
          fog_storage_services = create_connection_with_access(storage_access_key, data)

          directory = fog_storage_services.directories.get(data[:container_name])
          if (directory.destroy)
            render json: {status: 'SUCCESS', message: 'Container Deleted Successfully', Response: directory}, status: :ok
          end
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Storage Container not Deleted due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will create the connection with existing storage account
      private def create_connection(data)
        cr = ComputeResource.find(data[:compute_resource_id])
        fog_storage_service = Fog::Storage::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_storage_service
      end

      #this function will create the connection with access_key storage account
      private def create_connection_with_access(storage_access_key, data)
        cr = ComputeResource.find(data[:compute_resource_id])
        fog_storage_service = Fog::Storage::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            azure_storage_account_name: data[:storage_account_name],
            azure_storage_access_key: storage_access_key
        )
        return fog_storage_service
      end
    end
  end
end

