require 'aws-sdk'

module Api
  module V2
    class ApigatewayController < ::ApplicationController
      skip_before_action :verify_authenticity_token

      def create
        data = params.permit(:api_name, :endpoint_type, :description, :compression_size, :compute_resource_id)
        begin
          client = create_connection(data)
          resp = client.create_rest_api({
                                            name: data[:api_name], # required
                                            description: data[:description],
                                            minimum_compression_size: data[:compression_size],
                                            api_key_source: "HEADER",
                                            endpoint_configuration: {
                                                types: [data[:endpoint_type]],
                                            },
                                        })
          render json: {status: 'SUCCESS', message: 'Aws Api GateWay Creation Successfully', response: resp.to_hash.to_json}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While Creating Aws Api GateWay due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      def destroy
        data = params.permit(:resource_id, :compute_resource_id)
        begin
          client = create_connection(data)
          response = client.delete_rest_api({
                                                rest_api_id: data[:resource_id], # required
                                            })
          ::Foreman::Logging::logger('testpro').info response
          render json: {status: 'SUCCESS', message: 'Aws ApiGateWay Deletion Successfully', response: response.to_hash.to_json}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Problem While Deleting Aws Api GateWay due to #{e.message}"}, status: :unprocessable_entity
        end
      end


      #this function will create the connection with SNS AWS
      private def create_connection(data)
        cr = ComputeResource.find(data[:compute_resource_id])
        client = Aws::APIGateway::Client.new(
            :access_key_id => cr.user,
            :secret_access_key => cr.password, :region => cr.url)
        return client
      end
    end
  end
end

