require 'aws-sdk-s3'

module Api
  module V2
    class S3Controller < ApplicationController
      skip_before_action :verify_authenticity_token

      def getComputeResourceValue(id)
        cr = ComputeResource.find(id);
        return cr
      end

      def create
        begin
          data = params.permit(:bucket_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])

          s3 = Aws::S3::Client.new(:access_key_id => cr.user,
                                   :secret_access_key => cr.password, :region => cr.url
          )
          resp = s3.create_bucket(bucket: data[:bucket_name])
          #adding tags
          s3.put_bucket_tagging({
                                    bucket: data[:bucket_name],
                                    tagging: {tag_set: [{key: "Name", value: data[:bucket_name]}]}
                                })
          ::Foreman::Logging::logger('testpro').info resp
          render json: {status: 'SUCCESS', message: 'S3 bucket Created', response: resp.to_hash, createdOn: Time.now.strftime("%Y-%m-%d %H:%M:%S")}, status: :ok
        rescue Exception => e
          render json: {status: 'FAILURE', message: 'Error in creation S3', response: "#{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

      def exists
        begin
          cr = ComputeResource.find_by type: 'Foreman::Model::EC2'
          s3 = Aws::S3::Client.new(:access_key_id => cr.user,
                                   :secret_access_key => cr.password, :region => cr.url)
          bucket_exists = Aws::S3::Bucket.new(:name => params[:id], :client => s3)
          if bucket_exists.exists?
            response = {'status' => 'SUCCESS', 'message' => 'exists'}
          else
            response = {'status' => 'SUCCESS', 'message' => 'not exists'}
          end
          render json: response, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'FAILURE', message: 'Error in checking bucket exists', response: "#{e}"}, status: :unprocessable_entity
        end
      end

      def destroy
        begin
          data = params.permit(:bucket_name, :compute_resource_id)
          cr = ComputeResource.find(data[:compute_resource_id])

          s3 = Aws::S3::Client.new(:access_key_id => cr.user,
                                   :secret_access_key => cr.password, :region => cr.url
          )
          object_list = s3.list_objects_v2(bucket: data["bucket_name"])
          files = []
          object_list.contents.each do |file|
            files.push({'key': file.key})
          end
          ::Foreman::Logging::logger('testpro').info files
          if files.count != 0
            deletion_response = s3.delete_objects(bucket: data["bucket_name"], delete: {objects: files})
            ::Foreman::Logging::logger('testpro').info deletion_response
          end
          response = s3.delete_bucket(bucket: params[:id])
          ::Foreman::Logging::logger('testpro').info response
          render json: {status: 'SUCCESS', message: 'S3 deleted successfully', response: response.to_hash.to_json}, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "S3 not deleted due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end

    end
  end
end




