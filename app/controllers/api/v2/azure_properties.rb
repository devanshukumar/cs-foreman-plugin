require 'rest-client'
require 'json'
require 'fog/azurerm'


class AzureConnector
  attr_accessor :subscriptionId, :version, :resource, :provider

  def initialize(client_id, client_secret, tenant_id, subscriptionId)
    @client_id = client_id
    @client_secret = client_secret
    @tenant_id = tenant_id
    @subscriptionId = subscriptionId
  end

  def get_appgateway_payload(appname, res_group, pip, agsubnet, vnet, location, subscription_id, options)

    payload = {
        "properties": {
            "sku": {
                "name": "#{options[:sku_name]}",
                "tier": "#{options[:sku_tier]}",
                "capacity": options[:sku_capacity]
            },
            "gatewayIPConfigurations": [
                {
                    "properties": {
                        "subnet": {
                            "id": "/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/virtualNetworks/#{vnet}/subnets/#{agsubnet}"
                        }
                    },
                    "name": "myAGIPConfig",
                    "id": ""
                }
            ],
            "authenticationCertificates": [],
            "sslCertificates": [],
            "frontendIPConfigurations": [
                {
                    "properties": {
                        "publicIPAddress": {
                            "id": "/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/publicIPAddresses/#{pip}"
                        }
                    },
                    "name": "myAGFrontendIPConfig",
                    "id": ""
                }
            ],
            "frontendPorts": [
                {
                    "properties": {
                        "port": 80
                    },
                    "name": "myFrontendPort",
                    "id": ""
                }
            ],
            "probes": [],
            "backendAddressPools": [
                {
                    "properties": {
                        "backendIPConfigurations": [],
                        "backendAddresses": []
                    },
                    "name": "myAGBackendPool",
                    "id": ""
                }
            ],
            "backendHttpSettingsCollection": [
                {
                    "properties": {
                        "port": 80,
                        "protocol": "Http",
                        "cookieBasedAffinity": "Enabled",
                        "requestTimeout": 120,
                        "authenticationCertificates": [],
                        "probeEnabled": false
                    },
                    "name": "myPoolSettings",
                    "id": ""
                }
            ],
            "httpListeners": [
                {
                    "properties": {
                        "frontendIPConfiguration": {
                            "id": "/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/applicationGateways/#{appname}/frontendIpConfigurations/myAGFrontendIPConfig"
                        },
                        "frontendPort": {
                            "id": "/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/applicationGateways/#{appname}/frontendPorts/myFrontendPort"
                        },
                        "protocol": "Http",
                        "requireServerNameIndication": false
                    },
                    "name": "myAGListener",
                    "id": ""
                }
            ],
            "urlPathMaps": [],
            "requestRoutingRules": [
                {
                    "properties": {
                        "ruleType": "Basic",
                        "backendAddressPool": {
                            "id": "/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/applicationGateways/#{appname}/backendAddressPools/myAGBackendPool"
                        },
                        "backendHttpSettings": {
                            "id": "/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/applicationGateways/#{appname}/backendHttpSettingsCollection/myPoolSettings"
                        },
                        "httpListener": {
                            "id": "/subscriptions/#{subscription_id}/resourceGroups/#{res_group}/providers/Microsoft.Network/applicationGateways/#{appname}/httpListeners/myAGListener"
                        }
                    },
                    "name": "rule1",
                    "id": ""
                }
            ],
            "redirectConfigurations": []
        },
        "location": "#{location}"
    }
    return payload
  end

  def get_network_connector
    return fog_network_service = Fog::Network::AzureRM.new(
        tenant_id: @tenant_id,
        client_id: @client_id,
        client_secret: @client_secret,
        subscription_id: @subscriptionId,
        environment: 'AzureCloud'
    )
  end

  def get_token
    token_url = "https://login.windows.net/" + @tenant_id + "/oauth2/token"
    resp = RestClient.post(
        token_url,
        :grant_type => 'client_credentials',
        :client_id => @client_id,
        :client_secret => @client_secret,
        :resource => 'https://management.azure.com'
    )

    return 'Bearer ' + JSON.parse(resp)['access_token']
  end

  def get_item
    token = get_token
    url = "https://management.azure.com/subscriptions/#{@subscriptionId}/providers/#{@provider}/#{@resource}?api-version=#{@version}"
    resp = RestClient.get(
        url,
        #        :content_type => 'application/json',
        :authorization => token
    )
    return JSON.parse(resp.body)
  end

  def create_item(name, res_group, payload)
    token = get_token
    url = "https://management.azure.com/subscriptions/#{@subscriptionId}/resourceGroups/#{res_group}/providers/#{@provider}/#{resource}/#{name}?api-version=#{@version}"
    resp = RestClient.put(
        url,
        payload,
        {:authorization => token}
    )
    return JSON.parse(resp.body)

  end

  def create_items(url, payload)
    token = get_token

    resp = RestClient.put(
        url,
        payload,
        {:authorization => token, :Content - Type => application / json}
    )
    return JSON.parse(resp.body)

  end
end
