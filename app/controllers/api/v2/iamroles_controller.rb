require 'aws-sdk-iam' # v2: require 'aws-sdk'

module Api
  module V2
    class IamrolesController < ApplicationController
      skip_before_action :verify_authenticity_token


      def index
        begin
          cr = ComputeResource.find_by type: 'Foreman::Model::EC2'
          @iam_client = Aws::IAM::Client.new(:access_key_id => cr.user,
                                             :secret_access_key => cr.password, :region => cr.url)
          role_names = []
          role_arns = []
          roles = @iam_client.list_roles({})
          roles[0].each do |role|
            role_names << role.role_name
            role_arns << role.arn

          end
          render json: {status: 'SUCCESS', message: 'IAM roles fetched',
                        response: {role_names: role_names, role_arns: role_arns}
          }, status: :ok
        rescue Exception => e
          render json: {status: 'ERROR', message: "IAM roles not fetched due to #{e}"}, status: :unprocessable_entity
          ::Foreman::Logging::logger('testpro').error e
        end
      end
    end
  end
end
