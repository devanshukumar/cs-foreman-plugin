require 'fog/azurerm'

module Api
  module V2
    class ResourcegroupController < ::ApplicationController

      skip_before_action :verify_authenticity_token

      def create
        data = params.permit(:resource_group_name, :location, :environment, :end_point_url, :compute_resource_id, :organization_id)

        begin
          fog_storage_service = create_connection()
          if (fog_storage_service.resource_groups.check_resource_group_exists(data[:resource_group_name]))
            render json: {status: 'ERROR', message: 'Resource Group Already Exist'}, status: :unprocessable_entity
          else
            resp = fog_storage_service.resource_groups.create(
                name: data[:resource_group_name],
                location: data[:location]
            )
            render json: {status: 'SUCCESS', message: 'Resource Group Created Successfully', Response: resp}, status: :ok
          end
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Resource Group Not Created due to #{e.message}"}, status: :unprocessable_entity
        end
      end

      #this function will create the connection with existing storage account
      private def create_connection()
        cr = ComputeResource.find_by type: 'ForemanAzureRM::AzureRM'
        fog_storage_service = Fog::Resources::AzureRM.new(
            tenant_id: cr.uuid,
            client_id: cr.url,
            client_secret: cr.password,
            subscription_id: cr.user,
            environment: 'AzureCloud'
        )
        return fog_storage_service
      end

      def destroy

        data = params.permit(:resource_group_name, :location, :environment, :end_point_url, :compute_resource_id, :organization_id)
        begin
          fog_storage_service = create_connection()
          resource_group = fog_storage_service.resource_groups.get(data[:resource_group_name])
          resp = resource_group.destroy
          render json: {status: 'SUCCESS', message: 'Resource Group Destroy Successfully', Response: resp}, status: :ok
        rescue Exception => e
          ::Foreman::Logging::logger('testpro').error e
          render json: {status: 'ERROR', message: "Resource Group Not Created due to #{e.message}"}, status: :unprocessable_entity
        end
      end
    end
  end
end

