Rails.application.routes.draw do
  get 'new_action', to: 'testpro/hosts#new_action'

  namespace :api, :defaults => {:format => 'json'} do
    scope "(:apiv)", :module => :v2, :defaults => {:apiv => 'v2'}, :apiv => /v2/,
          :constraints => ApiConstraints.new(:version => 2) do
      resources :sns, :only => [:create,:destroy,:index]
      resources  :iamroles, :only => [:index]
      resources  :iampolicies, :only => [:index]
      resources :disk, :only => [:index, :create, :show, :destroy]
      resources :storageaccount, :only => [:create, :destroy]
      resources :vpngateway, :only => [:create, :destroy]
      resources :tablestore, :only => [:create, :destroy]
      resources :containerstore, :only => [:create, :destroy]
      resources :appgateway, :only => [:create, :destroy]
      resources :filestore, :only => [:index, :create,:destroy]
      resources :storageaccountlist, :only => [:index]
      resources :resourcegroupname, :only => [:index]
      resources :dbpaas, :only => [:create, :destroy]
      resources :vnet, :only => [:create, :destroy]
      resources :lambda, :only => [:create,:destroy]
      resources :s3 do
        resources :items, only: [:create, :destroy]
        get :exists, on: :member
      end
      resources :nsg do
        get :list, on: :member
      end
      resources :ses do
        resources :items, only:  [:create]
        post   'add_domain', on: :member
        post   'add_email_address', on: :member
        delete 'delete_identity', on: :member
      end
      resources :apigateway, :only => [:create,:destroy]
      resources :vnetapi, :only => [:show]
      resources :awsdynamodb, :only => [:create,:destroy]
      resources :amazonredshiftcluster, :only => [:create,:destroy]
      resources :efs, :only => [:create,:destroy]
      resources :iamusers, :only => [:create,:destroy]
      resources :iamgroups, :only => [:create,:destroy,:index]
      resources :certificates, :only => [:create,:destroy]
      resources :ebs do
        post :createEBSAppEnvironment, on: :member
        post :updateApplicationVersion, on: :member
        post :updateApplicationDescription, on: :member
        get :get_solution_stacks, on: :member
          
        post :deleteApplicationnEnvConfig, on: :member
        post :deleteEBSApplication, on: :member
        post :deleteApplicationVersion, on: :member

      end
      resources :util do
        post :set_cr_value, on: :member
      end 
      resources :rds do
        post :deleteRDS, on: :member
        post :createSnapshot, on: :member
        post :restoreSnapshot, on: :member
        post :restoreLatestSnapshot, on: :member
        get :listSnapshot, on: :member

      end

      resources :azure do
      #  post :createAzureSQLDB, on: :member
       # post :createAuzreSQLServer, on: :member
       # post :createvnet, on: :member
        post :createVirtualNetworkGateway, on: :member
#    post :createPublicIp, on: :member
        post :expressRouteCircuit, on: :member
      #  post :deletevnet, on: :member
        post :deleteExpressRoute, on: :member

      end

      resources :aws do
        get :listS3Bucket, on: :member
        post :createStorageBucket, on: :member
        post :listS3BucketKeys, on: :member
        post :createEFS, on: :member
        post :createUserLogin, on: :member

      end
       ####listing API
resources :subnetapi do
    post :showSubnets, on: :member
  end
  

    end
  end
end


